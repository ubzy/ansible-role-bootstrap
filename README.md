# Ansible Role: Boostrap

[![Build Status](https://travis-ci.org/UbzyHD/ansible-role-bootstrap.svg?branch=master)](https://travis-ci.org/UbzyHD/ansible-role-bootstrap)

## Contents
- [Ansible Role: Boostrap](#Ansible-Role-Boostrap)
  - [Contents](#Contents)
  - [Description](#Description)
  - [Role Variables](#Role-Variables)
  - [Example Playbook](#Example-Playbook)
  - [License](#License)
  - [Author Information](#Author-Information)
  - [Attributes](#Attributes)

## Description

A bootstrap role to provision either virtual or physical servers that have been freshly deployed.

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

- User setup information:

| Variable        | Default Value           | Description / Additional Info  |
| --------------- |:-----------------------| ------------|
| username           | ubzy | Account name of Linux user account |
| hashed_password    | \$6\$vN9O35ZeJIRSUZ$b5/b3M2536JeDPFM7OPuqb/rqF7pyiNFdvOQ5S0PluMXRZg1ydH/JUyAyVuCymZ/HfcFtUowuynaaqgFk.NzA0      | To create a hashed password, ensure the package `whois` is installed then execute the command: `mkpasswd -m sha-512`and then enter the password you would like to use to sign into your normal linux user account.  |
| ssh_publickey_link | https://github.com/ubzyhd.keys      | SSH Public Key to be added to the created user account   |

- Fail2Ban configuration:

| Variable           | Default Value        | Description / Additional Info  |
| ------------------ |----------------------| ------------|
| fail2ban_maxretry  | 1                    | Number of attempts before IP is banned - 1 = If an IP triggers a rule once then it is banned |
| fail2ban_bantime   | -1                   | How long IP is banned for. -1 = Banned forever  |
| fail2ban_findtime  | 432000               | Amount in seconds for F2B to detect violations in a given time frame   |
| fail2ban_banaction | nftables-allports    | How Fail2Ban should ban offending IP's. On a system with F2B installed, run `ls /etc/fail2ban/action.d/`   |

- GRUB configuration:

| Variable  | Default Value  | Description / Additional Info  |
| --------- |----------------| -------------------------------|
| name      | "GRUB_TIMEOUT" | How long GRUB should be displayed before booting into OS |
| value     | 2              | Set to 2 seconds  |


- Timezone:

| Variable      | Default Value  | Description / Additional Info  |
| ------------- |----------------| -------------------------------|
| timezone      | Europe/London  | Set this to the physical location of the server. Use this [list of timezones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones#List) and adjust accordingly |

And here are all these variable ready to be pasted into a playbook. Edit accordingly.

```
  vars:
    # Normal user account details
    username: ubzy
    hashed_password: $6$vN9O35ZeJIRSUZ$b5/b3M2536JeDPFM7OPuqb/rqF7pyiNFdvOQ5S0PluMXRZg1ydH/JUyAyVuCymZ/HfcFtUowuynaaqgFk.NzA0
    ssh_publickey_link: https://github.com/ubzyhd.keys

    # Fail2Ban settings
    fail2ban_maxretry: 1
    fail2ban_bantime: -1
    fail2ban_findtime: 43200
    fail2ban_banaction: nftables-allports

    # GRUB settings
    grub_settings:
      - name: "GRUB_TIMEOUT"
        value: "2"

    # Timezone
    timezone: Europe/London
```

## Example Playbook

An example playbook can be found in the root of the repository named `example-playbook-bootstrap.yml` which contains all the variables used in this role.

## License

MIT / BSD

## Author Information

Ansible role created by [UbzyHD](https://github.com/ubzyhd)

## Attributes

This role wouldn't have been possible for me to create without the following people:

| Author                                          | Website                                                       |
| ----------------------------------------------- |---------------------------------------------------------------|
| [Jeff Geerling](https://github.com/geerlingguy) | [https://www.jeffgeerling.com/](https://www.jeffgeerling.com) |
